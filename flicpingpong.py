#!/usr/bin/env python
#
import webapp2
import os
import logging
from google.appengine.ext import ndb
import json
import uuid


ADD_SCORE = "addscore"
ADD_PLAYER = "addplayer"
SET_PLAYER_NUMBER = "setplayernumber"
GET_SCORES = "getscores"
RESET_SCORE = "resetscore"
SET_NEXT_PLAYER = "setnextplayer"


logging.basicConfig(level=logging.DEBUG)

class PlayerNumber(ndb.Model):
    playerNumber = ndb.IntegerProperty()
    playerId = ndb.StringProperty()
    
class Player(ndb.Model):
    playerId = ndb.StringProperty()
    playerName = ndb.StringProperty()
    score = ndb.IntegerProperty()

class PingPongHandler(webapp2.RequestHandler):
    def __init__(self, *args, **kwargs):
        super(PingPongHandler, self).__init__(*args, **kwargs)
        self.logger = logging.getLogger("PingPongHandler")
        self.logger.setLevel(logging.DEBUG)
        
    def get(self):
        action = self.request.GET['action'].lower().strip()
        self.logger.info("action: " + action)
        
        if action == ADD_SCORE:
            self.logger.info("adding score")
            self.addScore(int(self.request.GET['playerNumber']))
            
        if action == ADD_PLAYER:
            self.logger.info("adding player")
            self.addPlayer(self.request.GET['name'])
            
        if action == SET_PLAYER_NUMBER:
            self.logger.info("setting player number")
            self.setPlayerNumber(self.request.GET['playerId'], int(self.request.GET['playerNumber']))
            
        if action == GET_SCORES:
            self.logger.info("getting scores")
            self.getScores()
        
        if action == RESET_SCORE:
            self.logger.info("resetting scores")
            self.resetScore(int(self.request.GET['playerNumber']))
            
        if action == SET_NEXT_PLAYER:
            self.logger.info("setting next player")
            self.setNextPlayer(int(self.request.GET['playerNumber']))
        

    def getPlayerById(self, playerId):
        query = Player.query()
        for p in query.fetch():
            if p.playerId == playerId:
                return p
        return None

    def getPlayerByNumber(self, playerNumber):
        matches = [pn for pn in PlayerNumber.query().fetch() if pn.playerNumber == playerNumber]
        playerId = -1
        if len(matches) > 0:
            playerId = matches[0].playerId
        logging.info("playerId: " + str(playerId))
        
        player = [p for p in Player.query().fetch() if p.playerId == playerId][0]
        return player
    
    def addScore(self, playerNumber):
        player = self.getPlayerByNumber(playerNumber)
        if player is not None:
            player.score += 1
            player.put()
            self.logger.info("added score")
        else:
            self.logger.info("found no player with player number " + playerNumber)

    def addPlayer(self, playerName):
        player = Player()
        player.playerId = str(uuid.uuid4())
        player.playerName = playerName
        player.score = 0
        player.playerNumber = -1
        player.put()
        self.logger.info("added player")
        
    
    def setPlayerNumber(self, playerId, playerNumber):
        for pn in PlayerNumber.query().fetch():
            if pn.playerNumber == playerNumber:
                pn.playerId = playerId
                pn.put()
                return 
        pn = PlayerNumber()
        pn.playerNumber = playerNumber
        pn.playerId = playerId
        pn.put()
            
    def getScores(self):
        playerIds = dict([(pn.playerId, pn.playerNumber) for pn in PlayerNumber.query().fetch()])
        self.logger.info("playerIds: " + str(playerIds))
        
        scores = []
        for p in Player.query().fetch():
            if p.playerId in playerIds.keys():
                scores.append({"playerNumber" : playerIds[p.playerId], "playerName": p.playerName, "score" : p.score})
        scores.sort(key = lambda x: x['playerNumber'])
        self.response.write(json.dumps(scores))
        
    def resetScore(self, playerNumber):
        player = self.getPlayerByNumber(playerNumber)
        player.score = 0
        player.put()
        logging.info("reset score")
    
    def setNextPlayer(self, playerNumber):
        matches = [pn for pn in PlayerNumber.query().fetch() if pn.playerNumber == playerNumber]
        playerId = -1
        if len(matches) > 0:
            playerId = matches[0].playerId
        logging.info("playerId: " + str(playerId))
        
        players = [p for p in Player.query().fetch()]
        players.sort(key=lambda p: p.playerName)
        
        index = -1
        for i, player in enumerate(players):
            if player.playerId == playerId:
                index = i
            
        logging.info("index: " + str(index))
        index += 1
        if index >= len(players):
            index = 0
            
        self.setPlayerNumber(players[index].playerId, playerNumber)
            
            
    def getAllPlayers(self):
        query = Player.query()
        players = [p for p in query.fetch()]
        players.sort(key = lambda x: x.playerName)
        return players
            
            